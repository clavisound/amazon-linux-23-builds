I made those scripts to use github which is IPv4 only with my IPv6 only amazon linux server.

Put here source files for tor and torsocks and run the scripts.

After starting tor, in another terminal run `source torsocks on`. With `torsocks sh` you can verify that LD_PRELOAD has a torsocks library. Now you start any program and the traffic will go through tor.

Some or ALL of those values are needed to connect to a tor network via IPv6 only client

Add those to torrc if missing.
```
AssumeReachableIPv6 1
ClientPreferIPv6DirPort 1
ClientPreferIPv6ORPort 1
ClientUseIPv6 1
```